package ferzle.util;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * An iterator for the Node class.
 * 
 * @author Chuck Cusack
 * @version 1.0, February 2008
 */
public class NodeIterator<T> implements Iterator<T>
{
    Node<T> theNode;
    
    public NodeIterator(Node<T> theNode) {
        this.theNode=theNode;
    }
    public boolean hasNext() {
        return theNode!=null;
    }
    
    public T next() {
        if(!hasNext()) {
            throw new NoSuchElementException();
        }
        T key = theNode.getKey();
        theNode=theNode.getNext();
        return key;
    }
    
    // We won't implement this operation.  It isn't required.
    public void remove() throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }
    
}
