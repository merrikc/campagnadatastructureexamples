package ferzle.util;

/**
 * A simple Node class for use in a linked list type structure
 * 
 * @author Chuck Cusack
 * @version 1.0, March 2013
 */
public class DoublyLinkedNode<T> {
	private T					key;
	private DoublyLinkedNode<T>	previous;
	private DoublyLinkedNode<T>	next;

	public DoublyLinkedNode() {
		previous = null;
		next = null;
	}

	public DoublyLinkedNode(T key) {
		this.key = key;
		previous = null;
		next = null;
	}

	public DoublyLinkedNode(T key, DoublyLinkedNode<T> previous, DoublyLinkedNode<T> next) {
		this.previous = previous;
		this.next = next;
		this.key = key;
	}

	public DoublyLinkedNode<T> getPrevious() {
		return previous;
	}

	public void setPrevious(DoublyLinkedNode<T> previous) {
		this.previous = previous;
	}

	public DoublyLinkedNode<T> getNext() {
		return next;
	}

	public void setNext(DoublyLinkedNode<T> next) {
		this.next = next;
	}

	public T getKey() {
		return key;
	}

	public void setKey(T key) {
		this.key = key;
	}
}
