package ferzle.queue;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * A simple queue class based on an ArrayList
 * 
 * @author Chuck Cusack
 * @version 1.0, February 2008
 */
public class ArrayListQueue<T> implements QueueInterface<T> {
	
	// The data
   ArrayList<T> theElements;
   
    public ArrayListQueue() {
        theElements = new ArrayList<T>();
    }

	@Override
    public boolean isEmpty() {
    	// TODO Implement me!
    	if(theElements.size()!=0)
    	{
    		return false;
    	}
    	else 
    	{
    		return true;
    	}
    }

	@Override
    public boolean isFull() {
    	// TODO Implement me!
    	return false;
    }

	@Override
    public boolean enqueue(T item) {
    	// TODO Implement me!
	
		return theElements.add(item);
		
    }

	@Override
    public T dequeue() {
    	// TODO Implement me!
		if(!isEmpty()){
			return  theElements.remove(0);
		}
			return null;
		
    } 

	@Override
    public T peek() {
    	// TODO Implement me!
		if(!isEmpty())
		{
    	return theElements.get(0);
		}
		else
		{
			return null;
		}
    }

	@Override
	public Iterator<T> iterator() {
		// TODO Implement me if time permits.
		return theElements.iterator();
	}
}
