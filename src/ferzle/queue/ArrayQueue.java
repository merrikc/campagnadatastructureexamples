package ferzle.queue;

import java.util.Iterator;

/**
 * A simple Queue implementation based on an array
 * 
 * @author Chuck Cusack
 * @version 1.0, February 2008
 */
public class ArrayQueue<T> implements QueueInterface<T> {
	
	// The data
	T[] theElements;
	private int cap=0;
// ITS WRONG should be circular array thing have head and tail thing 
	public ArrayQueue(int capacity) {
		// The following code is illegal in Java.
		// You cannot create an array of a generic type in Java
		// theElements = new T[capacity];

		// There are two "hacks" that allow you to get an array of
		// generic type. Both give compiler warnings.
		theElements = (T[]) new Object[capacity];
		
		// theElements =
		// (T[])Array.newInstance(theElements.getClass(),capacity);
	}

	@Override
	public boolean isEmpty() {
		// TODO Implement me!
		if(theElements.length!=0)
		{
			return true;
		}
		
		return false;
	}

	@Override
	public boolean isFull() {
		// TODO Implement me!
		/*
		 * boolean full=false; try { T[] newElements = theElements.clone();
		 * newElements[newElements.length]=(T) "";
		 * 
		 * 
		 * }catch (ArrayIndexOutOfBoundsException e) { full=true; }
		 * 
		 * return full;
		 */
		return (cap>=theElements.length-1);
	}

	@Override
	public boolean enqueue(T item) {
		// TODO Implement me!
		if(isFull())
		{
			return false;
		}
		else
		{
			for(int i=theElements.length-1;i>=0;i--)
			{
				theElements[i+1]=theElements[i];
			}
			theElements[0]=item;
			cap++;
		return true;
		}
	}

	@Override
	public T dequeue() {
		// TODO Implement me!
		T n= theElements[theElements.length-1];
		
		 theElements[theElements.length-1] = null;
		 cap--;
		 return n;
	}

	@Override
	public T peek() {
		// TODO Implement me!
		return theElements[theElements.length-1];
	}

	@Override
	public Iterator<T> iterator() {
		// TODO Implement me if time permits.
		return null;
	}
}
